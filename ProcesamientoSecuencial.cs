﻿namespace BatchProcessingWithThreads;

/// <summary>
/// Clase que encapsula el procesamiento secuencial de archivos.
/// </summary>
class ProcesamientoSecuencial : IProcesamientoArchivos
{
    private readonly int _totalArchivos;

    /// <summary>
    /// Constructor de la clase ProcesamientoSecuencial.
    /// </summary>
    /// <param name="totalArchivos">La cantidad total de archivos a procesar secuencialmente.</param>
    public ProcesamientoSecuencial(int totalArchivos)
    {
        _totalArchivos = totalArchivos;
    }

    /// <summary>
    /// Método para procesar archivos secuencialmente.
    /// </summary>
    public void Procesar()
    {
        for (int archivo = 1; archivo <= _totalArchivos; archivo++)
        {
            Console.WriteLine($"Procesando archivo {archivo} secuencialmente...");
            Thread.Sleep(1000); // Simula un segundo de procesamiento
        }
    }
}