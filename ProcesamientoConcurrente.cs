﻿namespace BatchProcessingWithThreads;

/// <summary>
/// Clase que encapsula el procesamiento concurrente de archivos utilizando múltiples hilos.
/// </summary>
class ProcesamientoConcurrente : IProcesamientoArchivos
{
    private readonly int _totalArchivos;
    private readonly int _cantidadHilos;

    /// <summary>
    /// Constructor de la clase ProcesamientoConcurrente.
    /// </summary>
    /// <param name="totalArchivos">La cantidad total de archivos a procesar de forma concurrente.</param>
    /// <param name="cantidadHilos">La cantidad de hilos a utilizar para el procesamiento.</param>
    public ProcesamientoConcurrente(int totalArchivos, int cantidadHilos)
    {
        _totalArchivos = totalArchivos;
        _cantidadHilos = cantidadHilos;
    }

    /// <summary>
    /// Método para procesar archivos de forma concurrente utilizando múltiples hilos.
    /// </summary>
    public void Procesar()
    {
        int archivosPorHilo = _totalArchivos / _cantidadHilos;
        Thread[] hilos = new Thread[_cantidadHilos];

        for (int hiloIndice = 0; hiloIndice < _cantidadHilos; hiloIndice++)
        {
            int inicio = hiloIndice * archivosPorHilo + 1;
            int fin = (hiloIndice + 1) * archivosPorHilo;
            if (hiloIndice == _cantidadHilos - 1) fin = _totalArchivos;

            hilos[hiloIndice] = new Thread(() => ProcesarArchivosRango(inicio, fin));
            hilos[hiloIndice].Start();
        }

        foreach (Thread hilo in hilos)
        {
            hilo.Join();
        }
    }

    /// <summary>
    /// Método auxiliar para procesar archivos en un rango específico.
    /// </summary>
    /// <param name="inicio">El número de archivo inicial en el rango.</param>
    /// <param name="fin">El número de archivo final en el rango.</param>
    private void ProcesarArchivosRango(int inicio, int fin)
    {
        for (int archivo = inicio; archivo <= fin; archivo++)
        {
            Console.WriteLine($"Procesando archivo {archivo} con hilo {Thread.CurrentThread.ManagedThreadId}...");
            Thread.Sleep(1000); // Simula un segundo de procesamiento
        }
    }
}