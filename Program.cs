﻿using System.Diagnostics;

namespace BatchProcessingWithThreads;

/// <summary>
/// Clase principal que contiene el método de entrada del programa.
/// </summary>
class Program
{
    static void Main(string[] args)
    {
        int totalArchivos = 10;
        int cantidadHilos = 4;

        IProcesamientoArchivos procesoSecuencial = new ProcesamientoSecuencial(totalArchivos);
        IProcesamientoArchivos procesoConcurrente = new ProcesamientoConcurrente(totalArchivos, cantidadHilos);

        // Paso 1: Procesamiento secuencial
        Console.WriteLine("Procesamiento secuencial:");
        Stopwatch stopwatchSecuencial = new Stopwatch();
        stopwatchSecuencial.Start();
        procesoSecuencial.Procesar();
        stopwatchSecuencial.Stop();
        Console.WriteLine($"Tiempo total de procesamiento secuencial: {stopwatchSecuencial.ElapsedMilliseconds} ms");

        // Paso 2: Procesamiento con múltiples hilos
        Console.WriteLine("\nProcesamiento con múltiples hilos:");
        Stopwatch stopwatchConcurrente = new Stopwatch();
        stopwatchConcurrente.Start();
        procesoConcurrente.Procesar();
        stopwatchConcurrente.Stop();
        Console.WriteLine($"Tiempo total de procesamiento concurrente: {stopwatchConcurrente.ElapsedMilliseconds} ms");

        Console.WriteLine("\nPresiona cualquier tecla para salir...");
        Console.ReadKey();
    }
}