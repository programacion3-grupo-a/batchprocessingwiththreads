# Batch Processing With Threads

Este proyecto implementa un sistema de procesamiento de archivos en lotes utilizando hilos en C#. Proporciona dos enfoques para el procesamiento: secuencial y concurrente, con el objetivo de comparar la eficiencia entre ambos enfoques.

## Descripción

El proyecto consta de tres clases principales:

### `Program`

Clase principal que contiene el método de entrada del programa. Se encarga de iniciar el procesamiento secuencial y concurrente de archivos y medir el tiempo de ejecución de cada uno.

### `ProcesamientoSecuencial`

Clase que encapsula el procesamiento secuencial de archivos. Implementa la interfaz `IProcesamientoArchivos` y proporciona un método para procesar archivos uno por uno de manera secuencial.

### `ProcesamientoConcurrente`

Clase que encapsula el procesamiento concurrente de archivos utilizando múltiples hilos. Implementa la interfaz `IProcesamientoArchivos` y proporciona un método para procesar archivos en paralelo utilizando la cantidad especificada de hilos.

## Interfaces

### `IProcesamientoArchivos`

Interfaz que define el contrato para el procesamiento de archivos. Contiene un único método `Procesar()` que debe ser implementado por las clases que realizan el procesamiento de archivos.

## Uso

El programa se puede ejecutar con los siguientes pasos:

1. Especificar la cantidad total de archivos a procesar y la cantidad de hilos a utilizar en el método `Main()` de la clase `Program`.
2. Compilar y ejecutar el programa.
3. Se mostrará el tiempo total de procesamiento secuencial y concurrente de los archivos.