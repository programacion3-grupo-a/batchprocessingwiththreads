﻿namespace BatchProcessingWithThreads;

/// <summary>
/// Interfaz que define el contrato para el procesamiento de archivos.
/// </summary>
interface IProcesamientoArchivos
{
    /// <summary>
    /// Método para procesar archivos.
    /// </summary>
    void Procesar();
}